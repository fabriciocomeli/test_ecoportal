require_relative 'messages'
require_relative 'player'
require_relative 'movement'
require_relative 'validation'

class Game
  def initialize
    @board = %w[0 1 2 3 4 5 6 7 8]
    select_game_type
    create_players
  end

  def select_game_type
    puts "Choose a type: \n 1: Human vs Human\n 2: Computer vs Computer\n 3: Human vs Computer"
    @type_game = nil
    until @type_game
      @type_game = gets.chomp.to_i
      next if [1, 2, 3].include?(@type_game)

      puts "\nCommand Invalid!"
      puts "Choose a type: \n 1: Human vs Human\n 2: Computer vs Computer\n 3: Human vs Computer"
      @type_game = nil
    end
  end

  def create_players
    case @type_game
    when 1
      @player1 = Player.new(name: 'Player 1', player: Player::HUMAN, mark: 'O')
      @player2 = Player.new(name: 'Player 2', player: Player::HUMAN, mark: 'X')
    when 2
      @player1 = Player.new(name: 'Player 1', player: Player::COMPUTER, mark: 'O')
      @player2 = Player.new(name: 'Player 2', player: Player::COMPUTER, mark: 'X')
    when 3
      @player1 = Player.new(name: 'Player 1', player: Player::HUMAN, mark: 'O')
      @player2 = Player.new(name: 'Player 2', player: Player::COMPUTER, mark: 'X')
    end
  end

  def start_game
    # loop through until the game was won or tied
    process_movements until Movement.game_is_over(@board) || Validation.tie(@board)
  end

  def process_movements
    @players = [@player1, @player2]
    @players.each do |player|
      # start by printing the board
      Messages.print_board(@board)
      Messages.print_options

      check_player_mark(player)

      if Validation.tie(@board)
        Messages.print_is_over(@board)
        puts 'Draw!'
        break
      elsif Movement.game_is_over(@board)
        Messages.print_is_over(@board)

        if @player1.player == Player::COMPUTER && @player2.player == Player::COMPUTER
          puts "Computer #{player.name}, you won!"
        elsif @player1.player == Player::HUMAN && player == @player1
          puts "Congratulations #{player.name}, you won!"
        else
          puts 'You lost!'
        end
        break
      end
    end
  end

  def check_player_mark(player)
    puts "#{@player1.name} will be 'O' and the #{@player2.name} will be 'X'"

    if player.player == Player::HUMAN
      process_human_move(player)
    elsif !Movement.game_is_over(@board) && !Validation.tie(@board)
      process_computer_move(player)
    end
  end

  def process_human_move(player)
    Movement.get_human_spot(@board, player)
  end

  def process_computer_move(player)
    next_player = @players.reject { |p| p == player }.first

    Movement.eval_board(@board, next_player, player)
  end
end

game = Game.new
game.start_game
