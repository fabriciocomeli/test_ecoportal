class Player
  HUMAN = 'human'
  COMPUTER = 'computer'
  attr_reader :name, :player, :mark

  def initialize(name: nil, player: nil, mark: nil)
    @name = name
    @player = player
    @mark = mark
  end
end
