class Messages
  def self.print_board(board)
    puts " #{board[0]} | #{board[1]} | #{board[2]} \n===+===+===\n #{board[3]} | #{board[4]} | #{board[5]} \n===+===+===\n #{board[6]} | #{board[7]} | #{board[8]} \n"
    puts
  end

  def self.print_options
    puts 'Enter [0-8]:'
  end

  def self.print_is_over(board)
    puts '----------- ** Game Over ** -----------'
    print_board(board)
  end
end
