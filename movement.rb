class Movement
  def self.get_human_spot(board, player)
    puts "Round #{player.name}, choose the spot to mark: "
    spot = nil
    until spot
      spot = gets.chomp
      spot = if spot.nil? || !spot.match?(/[0-9]/)
               nil
             else
               spot.to_i
             end

      if !spot.nil? && board[spot] != 'X' && board[spot] != 'O'
        board[spot] = player.mark
      else
        puts 'Spot Invalid! Choose another spot: '
        spot = nil
      end
    end
  end

  def self.eval_board(board, player1, player2)
    puts
    spot = nil
    until spot
      if board[4] == '4'
        spot = 4
        board[spot] = player2.mark
      else
        spot = get_best_move(board, player2, player2, player1)
        if board[spot] != 'X' && board[spot] != 'O'
          board[spot] = player2.mark
        else
          spot = nil
        end
      end
    end
    puts '---------- * Computer turn * ----------'
    puts "Computer #{player2.name} marked spot #{spot}"
    puts
  end

  def self.get_best_move(board, _next_player, player1, player2, _depth = 0, _best_score = {})
    available_spaces = []
    best_move = nil
    board.each do |s|
      available_spaces << s if s != 'X' && s != 'O'
    end
    available_spaces.each do |as|
      board[as.to_i] = player1.mark
      if game_is_over(board)
        best_move = as.to_i
        board[as.to_i] = as
        return best_move
      else
        board[as.to_i] = player2.mark
        if game_is_over(board)
          best_move = as.to_i
          board[as.to_i] = as
          return best_move
        else
          board[as.to_i] = as
        end
      end
    end
    return best_move if best_move

    n = rand(0..available_spaces.count)
    available_spaces[n].to_i
  end

  def self.game_is_over(b)
    [b[0], b[1], b[2]].uniq.length == 1 ||
      [b[3], b[4], b[5]].uniq.length == 1 ||
      [b[6], b[7], b[8]].uniq.length == 1 ||
      [b[0], b[3], b[6]].uniq.length == 1 ||
      [b[1], b[4], b[7]].uniq.length == 1 ||
      [b[2], b[5], b[8]].uniq.length == 1 ||
      [b[0], b[4], b[8]].uniq.length == 1 ||
      [b[2], b[4], b[6]].uniq.length == 1
  end
end
