class Validation
  def self.tie(b)
    b.all? { |s| %w[X O].include?(s) }
  end
end
